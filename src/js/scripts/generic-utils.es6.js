const _ = require('-aek/utils');

const Config = require('../modules/config');

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/toLocaleString
const toLocalStringSupport = !!(typeof Intl === 'object' && Intl && typeof Intl.NumberFormat === 'function');

class GenericUtils {
  returnFormattedNumber(number) {
    if(toLocalStringSupport) {
      // NOTE - this needs to be set in the config.twig in order for this to work
      let webLanguage = Config.user.webLanguage;
      return parseInt(number).toLocaleString(webLanguage);
    } else {
      return number;
    }
  }

  // validityCheck is intended for complex objects, not booleans or integers. It will work on Strings due to how they work, however.
  validityCheck(input) {
    if(input !== undefined && input !== null) {
      return !_.isEmpty(input) || _.isArray(input);
    }

    return false;
  }

  capitaliseFirstLetter(input) {
    return String(input).charAt(0).toUpperCase() + String(input).slice(1);
  }

  // this will be superceded by the updated aek-config presets eventually
  mapCurrency(input, value) {
    value = parseFloat(value);
    switch(input) {
      case 'GBP':
        return '£' + value.toFixed(2);
      case '£':
        return '£' + value.toFixed(2);
      case 'USD':
        return '$' + value.toFixed(2);
      case 'AUD':
        return '$' + value.toFixed(2);
      case '$':
        return '$' + value.toFixed(2);
      case 'EUR':
        return '€' + value.toFixed(2);
      case '€':
        return '€' + value.toFixed(2);
      case 'RAN':
        return 'R' + value.toFixed(2);
      case 'R':
        return 'R' + value.toFixed(2);
      case 'TRY':
        return '₺' + value.toFixed(2);
      case '₺':
        return '₺' + value.toFixed(2);
    }
  }

  // at some point, lookup something that does this for all 140 colour codes
  lookupHexFromStringColour(colour) {
    switch(colour.toLowerCase()) {
      case 'red':
        return '#FF0000';
      case 'pink':
        return '#FFC0CB';
      case 'orange':
        return '#FFA500';
      case 'yellow':
        return '#FFFF00';
      case 'gold':
        return '#FFD700';
      case 'magenta':
        return '#FF00FF';
      case 'purple':
        return '#800080';
      case 'indigo':
        return '#4B0082';
      case 'green':
        return '#008000';
      case 'olive':
        return '#808000';
      case 'cyan':
        return '#00FFFF';
      case 'blue':
        return '#0000FF';
      case 'brown':
        return '#A52A2A';
      case 'maroon':
        return '#800000';
      case 'white':
        return '#FFFFFF';
      case 'gray':
        return '#808080';
      case 'black':
        return '#000000';
      default:
        return '#000000';
    }
  }

  returnLumaForHex(hexCode) {
    if(hexCode.substring(0, 1) !== '#') {
      hexCode = this.lookupHexFromStringColour(hexCode);
    }

    if(hexCode.length === 4) {
      // we have #fff or similar; parseInt below won't like that
      hexCode = hexCode.split('');
      hexCode = hexCode[0] + hexCode[1] + hexCode[1] + hexCode[2] + hexCode[2] + hexCode[3] + hexCode[3];
    }

    // http://stackoverflow.com/a/12043228/1306811 - calculating luminosity of colours
    let c = hexCode.substring(1);      // strip #
    let rgb = parseInt(c, 16);   // convert rrggbb to decimal
    let r = (rgb >> 16) & 0xff;  // extract red
    let g = (rgb >>  8) & 0xff;  // extract green
    let b = (rgb >>  0) & 0xff;  // extract blue

    let luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709
    return luma;
  }

  returnCommonPlural(count, input) {
    if(count && count !== null && this.validityCheck(input)) {
      let index = parseInt(count);
      if(index === 1) {
        return input;
      } else if(this.endsWith(input, 's')) {
        return input + '\'';
      } else {
        return input + 's';
      }
    } else {
      return null;
    }
  }

  endsWith(input, suffix) {
    if(input) {
      return input.indexOf(suffix, input.length - suffix.length) !== -1;
    } else {
      return false;
    }
  }

}

// export a single instance so all modules see the share the same thing
module.exports = new GenericUtils();
