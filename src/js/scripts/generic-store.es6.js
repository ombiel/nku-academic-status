const SimpleStore = require('-aek/simple-store');
const request = require('-aek/request');
const moment = require('moment');
const _ = require('-aek/utils');
const Notifier = require('-aek/notifier');

const Config = require('../modules/config');

// these can be disabled by commenting them out of the plugins array in the constructor()
const loggerPlugin = require('-aek/simple-store/plugins/logger');
const localStoragePlugin = require('-aek/simple-store/plugins/localstorage');

// i.e. 'aek2-test-project-key', this is defined as a JavaScript string explicitly in the JSON Config object
const storageKey = Config.storageKey;

class GenericStore extends SimpleStore {
  constructor() {
    super({
      initialState:{
        gpa:{
          gpaData: null,
          gpaError: null,
          $$$gpaLoading: false,
          $$$backgroundLoading: false,
          lastUpdated: null
        },
        regWindow:{
          regWindowData: null,
          regWindowError: null,
          $$$regWindowLoading: false,
          $$$backgroundLoading: false,
          lastUpdated: null
        }
      },
      plugins:[
        loggerPlugin(),
        localStoragePlugin(storageKey)
      ]
    });

    // you can modify state values here if you absolutely have to, i.e. if you're checking for something on startup
  }

  dispatchGpa(ctx, results, isError, errorMessage, loading, backgroundLoading) {
    // moment doesn't serialise well, so store the formatted string to re-use later
    let now = moment().format();
    ctx.dispatch({
      name: 'gpa',
      error: isError,
      extend:{
        gpaData: results,
        gpaError: errorMessage,
        $$$gpaLoading: loading,
        $$$backgroundLoading: backgroundLoading,
        lastUpdated: now
      }
    });
  }

  // an example of a default parameter value - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Default_parameters
  fetchGpa(backgroundLoading = false) {
    let ctx = this.context({ group:'GPA', path:'gpa' });

    // get any existing data from the store for if the service call fails
    let existingData = this.state.gpa.gpaData;
    this.dispatchGpa(ctx, existingData, false, null, true, backgroundLoading);

    request.action('get-gpa').end((err, res) => {
      if(!err && res && res.body !== null) {
          this.dispatchGpa(ctx, res.body.DATA, false, null, false, false);
      } else {
        let error = _.has(res, 'body') ? res.body : 'Unknown response from server.';
        this.dispatchGpa(ctx, existingData, true, error, false, false);
        this.genericNotifier('Server Failure', error, true, 'error');
      }
    });
  }

  dispatchRegWindow(ctx, results, isError, errorMessage, loading, backgroundLoading) {
    // moment doesn't serialise well, so store the formatted string to re-use later
    let now = moment().format();
    ctx.dispatch({
      name: 'regWindow',
      error: isError,
      extend:{
        regWindowData: results,
        regWindowError: errorMessage,
        $$$regWindowLoading: loading,
        $$$backgroundLoading: backgroundLoading,
        lastUpdated: now
      }
    });
  }

  // an example of a default parameter value - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Default_parameters
  fetchRegWindow(backgroundLoading = false) {
    let ctx = this.context({ group:'REG_WINDOW', path:'regWindow' });

    // get any existing data from the store for if the service call fails
    let existingData = this.state.regWindow.regWindowData;
    this.dispatchRegWindow(ctx, existingData, false, null, true, backgroundLoading);

    request.action('get-reg-window').end((err, res) => {
      if(!err && res && res.body !== null) {
          this.dispatchRegWindow(ctx, res.body.DATA, false, null, false, false);
      } else {
        let error = _.has(res, 'body') ? res.body : 'Unknown response from server.';
        this.dispatchRegWindow(ctx, existingData, true, error, false, false);
        this.genericNotifier('Server Failure', error, true, 'error');
      }
    });
  }

  // message takes plaintext only; HTML will be rendered as such
  // more examples of default parameters values
  genericNotifier(title, message, dismissable, level, autoDismiss = 0, clear = true) {
    if(clear) {
      Notifier.clear();
    }

    Notifier.add({
      title: title,
      message: message,
      dismissable: dismissable,
      level: level,
      autoDismiss: autoDismiss
    });
  }

  // children allows you to pass HTML to the Notifier to be displayed
  notifierWithChildren(title, children, dismissable, level, autoDismiss = 0, clear = true) {
    if(clear) {
      Notifier.clear();
    }

    Notifier.add({
      title: title,
      children: children,
      dismissable: dismissable,
      level: level,
      autoDismiss: autoDismiss
    });
  }

}

module.exports = new GenericStore();
