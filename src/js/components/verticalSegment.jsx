let React = require("react");
let Button = require('-components/button');
let Group = require('-components/group');
let _ = require('-aek/utils');

let VerticalSegment = React.createClass({
  getInitialState:function() {
    let expanded = this.props.expanded ? this.props.expanded : false;
    return {
      expanded:expanded
    };
  },

  componentWillReceiveProps:function(nextProps) {
    let expandedState = this.state.expanded;
    let existingProps = this.props;
    if(nextProps.expanded !== existingProps.expanded && expandedState !== nextProps.expanded) {
      this.setState({ expanded:nextProps.expanded });
    }
  },

  toggle:function() {
    let expanded = this.state.expanded;
    if(expanded) {
      this.setState({ expanded:false });
    } else {
      this.setState({ expanded:true });
    }
  },

  render:function() {
    let { data, sharp, buttonBelow, buttonText, buttonProps, style } = this.props;
    buttonProps = buttonProps ? buttonProps : {};

    let expandedState = this.state.expanded;
    let expandedClass = expandedState ? ' expanded' : '';

    let chevronIcon = expandedState ? 'chevron up' : 'chevron down';
    let sharpClasses = sharp ? ' sharp' : '';

    let iconRight = buttonText ? true : false;
    let buttonTextSpan = buttonText ? <span style={{ marginRight:'0.3rem' }}>{ buttonText }</span> : null;
    let buttonExpander =
      <Button icon={ chevronIcon } fluid onClick={ this.toggle } iconRight={ iconRight } key='expander-button'>
        { buttonTextSpan }
      </Button>;

    // might need pickHtmlProps here to filter buttonProps for any unsupported props
    let newProps = _.extend({}, buttonExpander.props, buttonProps);
    buttonExpander = React.cloneElement(buttonExpander, newProps);

    return (
      <Group style={ style } className={ 'drop-segment' + sharpClasses + expandedClass }>
        { !buttonBelow ? buttonExpander : null }
        { expandedState ? data : null }
        { buttonBelow ? buttonExpander : null }
      </Group>
    );
  }
});

module.exports = VerticalSegment;
