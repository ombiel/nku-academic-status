let React = window.React = require('react');
let reactRender = require('-aek/react/utils/react-render');
let { VBox } = require('-components/layout');
let Container = require('-components/container');
let BasicSegment = require("@ombiel/aek-lib/react/components/segment");
let {BannerHeader, Header} = require("@ombiel/aek-lib/react/components/header");
let Group = require("@ombiel/aek-lib/react/components/group");
let Panel = require("@ombiel/aek-lib/react/components/panel");

const GenericStore = require('./scripts/generic-store');
const Config = require('./modules/config');

let Screen = React.createClass({
  componentDidMount:function() {
    GenericStore.fetchGpa();
    GenericStore.fetchRegWindow();
    // this is required to trigger a rerender on SimpleStore update
    // only works thanks to React's fast virtual DOM; would impact performance otherwise
    GenericStore.on('change', () => {
      this.forceUpdate();
    });

    // endsWith fallback for IE - https://caniuse.com/#search=endswith
    // we also have a parallel implementation in GenericUtils if you want to avoid this
    if(!String.prototype.endsWith) {
      String.prototype.endsWith = function(suffix) {
        return this.indexOf(suffix, this.length - suffix.length) !== -1;
      };
    }
  },

  componentWillUnmount:function() {
    GenericStore.off('change');
  },

  render:function() {

    let gpa = null;
    let earnedHours = null;
    let academicStanding = null;
    let regWindowData = null;
    let regWindowItems = null;

    if (GenericStore.get('gpa.gpaData')){
      let data = GenericStore.get('gpa.gpaData');
      if (data.GPA && data.GPA !== ""){
        gpa = <Group>
          <BannerHeader flush textAlign='center' theme={Config.theme.primary}>{Config.language.gpaLabelText}</BannerHeader>
          <Header textAlign='center' level='2'>{data.GPA}</Header>
        </Group>;
      }
      if (data.EARNED_CREDITS_HOURS && data.EARNED_CREDITS_HOURS !== ""){
        earnedHours = <Group>
          <BannerHeader flush textAlign='center' theme={Config.theme.primary}>{Config.language.earnedHoursLabelText}</BannerHeader>
          <Header level='2' textAlign='center'>{data.EARNED_CREDITS_HOURS}</Header>
        </Group>;
      }
      if (data.ACADEMIC_STANDING && data.ACADEMIC_STANDING !== ""){
        academicStanding = <Group>
          <BannerHeader flush textAlign='center' theme={Config.theme.primary}>{Config.language.academicStandingLabelText}</BannerHeader>
          <Header level='2' textAlign='center'>{data.ACADEMIC_STANDING}</Header>
        </Group>;
      }
    }

    if (GenericStore.get('regWindow.regWindowData')){
      regWindowData = GenericStore.get('regWindow.regWindowData');

      regWindowItems = regWindowData.map((regWindow, index) => {
        return (
          <BasicSegment nopadding key={'reg-window-' + index}>
            <Group>
              <Header level='4'><u><b>{regWindow.TERM_TEXT}</b></u></Header>
              <Header level='4'>{Config.language.datesText}{regWindow.START_DATE}</Header>
            </Group>
          </BasicSegment>
        );
      });
    }

    return (
      <Container>
        <VBox>
          <Panel>
            <BasicSegment nopadding>
              <BasicSegment nopadding loading={GenericStore.get('gpa.$$$gpaLoading')}>
                {gpa}
                {earnedHours}
                {academicStanding}
              </BasicSegment>
              <a href={Config.regWindowUrl}>
                <BannerHeader flush textAlign='center' theme={Config.theme.primary}>{Config.language.regWindowLabelText}</BannerHeader>
                <BasicSegment loading={GenericStore.get('regWindow.$$$regWindowLoading')} >
                {regWindowItems}
                </BasicSegment>
              </a>
            </BasicSegment>
          </Panel>
        </VBox>
      </Container>
    );
  }
});

reactRender(<Screen />);
