const React = window.React = require('react');
const Page = require('-components/page');
const { BannerHeader } = require('-components/header');
const { Listview, Item } = require('-components/listview');
const { BasicSegment } = require('-components/segment');
const Notifier = require('-aek/notifier');

const _ = require('-aek/utils');

const GenericStore = require('../scripts/generic-store');
const Config = require('../modules/config');

let IndexPage = React.createClass({
  componentDidMount:function() {
    GenericStore.fetchCalendarViews();
  },

  componentWillUnmount:function() {
    Notifier.clear();
  },

  render:function() {
    let theme = Config.theme.primary;
    let altTheme = Config.theme.secondary;

    let data = GenericStore.get('views.viewData');
    let loading = GenericStore.get('views.$$$viewLoading');
    // this project currently isn't using this, but we use this as a visual switch so that we can apply different loading UI logic when data is loading silently in the background
    let backgroundLoading = GenericStore.get('views.$$$backgroundLoading');

    // NOTE - recommended reading - https://eslint.org/docs/rules/no-console
    console.log(data); // eslint-disable-line no-console

    let calViews = null;
    // as we know data is always an array, we can check for that explicitly
    // we also need to not display any existing view behind the loading state, if loading - it often pushes the loading widget off of the viewable page
    if(data.length > 0 && (!loading || backgroundLoading)) {
      // if data exists, convert the view object to an array we can push components to
      calViews = [];
      _.forEach(data, (item, index) => {
        // when pushing items to an array, they all require a unique key otherwise duplicate items will not be shown
        calViews.push(
          <Listview key={ 'view-' + index } style={{ marginTop:'1rem', borderRadius:'0' }}>
            <Item>{ item.desc }</Item>
          </Listview>
        );
      });
    }

    return (
      <Page>
        <BannerHeader theme={ theme } level='3' key='index-header' data-flex={0}>{ Config.language.pageName }</BannerHeader>
        <BasicSegment nopadding loading={ loading && !backgroundLoading }>
          { calViews }
        </BasicSegment>
      </Page>
    );
  }
});

module.exports = IndexPage;
